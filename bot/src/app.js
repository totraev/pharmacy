import config from './config'

import MessagesService from './services/MessagesService'
import PharmacyService from './services/PharmacyService'
import BasketService from './services/BasketService'

const { redis: { host, port }, pharmacyUrl, token } = config

const pharmacy = new PharmacyService(pharmacyUrl)
const basket = new BasketService(host, port)
const message = new MessagesService(token)

import {
  searchMsg,
  productMsg,
  notFoundMsg,
  errorMsg,
  setSearchOptions,
  setProductOptions,
  locationOptions,
  contactOptions,
  basketOptions,
  printPharmacy,
  pharmacyOptions
} from './helpers/message'


message.bot.on('text', (msg) => {
  const { text, chat: { id: chatId }} = msg

  if(text !== '/start')
    return

  basket.createBasket(chatId, (err) => {
    message.provideLocation(chatId)
  })
})

/**
 * Search products
 */
message.bot.on('text', function (msg) {
  const { text, message_id, chat: { id: chatId } } = msg

  if(msg.text === '/start' || msg.text === '\u{1F6D2} Basket')
    return

  basket.getBasket(chatId, (err, basket) => {
    if(!basket.location) {
      return message.provideLocation(chatId)
    }

    if(!basket.name | !basket.phone) {
      return message.provideContacts(chatId)
    }

    pharmacy.searchProducts(text, (err, results) => {
      if(results.length > 0) {
        message.displayProducts(chatId, results)
      } else {
        message.notFound(chatId)
      }
    })
  })
})


/**
 * Dispaly all or closest pharmacies for current product
 */
message.bot.on('callback_query', function(callbackQuery) {
  const { id, from: { id: chatId }, data } = callbackQuery
  const { key, name } = JSON.parse(data)

  if(key !== 'all' && key !== 'geo')
    return

  basket.getBasket(chatId, (err, basket) => {
    const location = key === 'geo' ?  basket.location : null

    pharmacy.displayPharmacies(name, location, (err, products) => {
      if(products.length > 0) {
        message.displayPharmacies(chatId, products, () => {
          message.answerCallbackQuery(id)
        })
      } else {
        message.notFound(chatId, () => {
          message.answerCallbackQuery(id)
        })
      }
    })
  })
})


/**
 * Add product to basket
 */
message.bot.on('callback_query', function(callbackQuery) {
  const { id, from: { id: chatId }, data } = callbackQuery
  const { id: productId, key } = JSON.parse(data)

  if(key !== 'add') return

  pharmacy.displayProduct(productId, (err, product) => {
    basket.addProduct(chatId, product, (err) => {
      message.addedToBasket(chatId, () => {
        message.answerCallbackQuery(id)
      })
    })
  })
})

/**
 * Set contacts
 */
message.bot.on('contact', (msg) => {
  const {
    contact: { phone_number, first_name, user_id },
    chat: {id: chatId}
  } = msg

  basket.setContact(chatId, first_name, phone_number, (err) => {
    message.inputDrugName(chatId)
  })
})

/**
 * Set location
 */
message.bot.on('location', (data) => {
  const { from: {id}, location: { latitude, longitude }} = data

  basket.setLocation(id, [latitude, longitude], (err) => {
    message.provideContacts(id)
  })
})

/**
 * Show basket
 */
message.bot.onText(/Basket$/, (msg) => {
  if(msg.text !== '\u{1F6D2} Basket') return
  const { chat: {id: chatId}} = msg

  basket.getBasket(chatId, (err, basket) => {
    message.showBasket(chatId, basket)
  })
})

/**
 * Remove order from basket
 */
message.bot.on('callback_query', function(callbackQuery) {
  const {id: msgId, from: { id: chatId }, data } = callbackQuery
  const { id, key } = JSON.parse(data)

  if(key !== 'remove') return

  basket.removeOrder(chatId, id, (err) => {
    message.removedFromBasket(chatId, () => {
      message.answerCallbackQuery(msgId)
    })
  })
})

/**
 * Making order
 */
message.bot.on('callback_query', function(callbackQuery) {
  const {id: msgId, from: { id: chatId }, data } = callbackQuery
  const { id, key } = JSON.parse(data)

  if(key !== 'order') return

  basket.getOrder(chatId, id, (err, order) => {
    pharmacy.createOrder(order, (err) => {
      basket.removeOrder(chatId, id, (err) => {
        message.orderCreated(chatId, () => {
          message.answerCallbackQuery(msgId)
        })
      })
    })
  })
})
