import TelegramBot from 'node-telegram-bot-api'
import Promise from 'bluebird'

import {
  searchMsg,
  productMsg,
  notFoundMsg,
  errorMsg,
  setSearchOptions,
  setProductOptions,
  locationOptions,
  contactOptions,
  basketOptions,
  printPharmacy,
  pharmacyOptions
} from '../helpers/message'

export default class MessagesService {
  constructor(token) {
    this.bot = new TelegramBot(token, { polling: true })
  }

  provideLocation(chatId, cb) {
    this.bot.sendMessage(chatId, `\u{1F4E2} Please provide your location`, locationOptions())
      .then(cb)
  }

  provideContacts(chatId, cb) {
    this.bot.sendMessage(chatId, `\u{1F4E2} Please provide your contacts`, contactOptions())
      .then(cb)
  }

  displayProducts(chatId, products, cb) {
    this.bot.sendMessage(chatId, '<b>PRODUCTS:</b>', { parse_mode: 'HTML' })
      .then(() => {
        return Promise.each(products, (product) => {
          return this.bot.sendMessage(chatId, searchMsg(product), setSearchOptions(product._id.substr(0, 20)))
        })
      })
      .then(() => this.bot.sendMessage(chatId, '------------------------------------'))
      .then(cb)
  }

  notFound(chatId, cb) {
    return this.bot.sendMessage(chatId, notFoundMsg())
      .then(cb)
  }

  displayPharmacies(chatId, pharmacies, cb) {
    this.bot.sendMessage(chatId, '<b>PHARMACIES:</b>', {parse_mode: 'HTML'})
      .then(() => {
        return Promise.each(pharmacies, (product) => {
          const [lng, lat] = product.location

          return this.bot.sendVenue(chatId, lat, lng, `\u{1F48A} ${product.pharmacy.name}`)
            .then(() => this.bot.sendMessage(chatId, productMsg(product), setProductOptions(product._id)))
        })
      })
      .then(() => this.bot.sendMessage(chatId, '------------------------------------'))
      .then(cb)
  }

  answerCallbackQuery(id) {
    this.bot.answerCallbackQuery(id)
  }

  addedToBasket(chatId, cb) {
    this.bot.sendMessage(chatId, '\u{1F4E2} Product has been added to basket', basketOptions())
      .then(cb)
  } 

  inputDrugName(chatId, cb) {
    this.bot.sendMessage(chatId, `\u{1F4E2} Input name of the drug you need!`, { reply_markup: { remove_keyboard: true }})
      .then(cb)
  }

  showBasket(chatId, basket, cb) {
    const { pharmacies, products } = basket

    const pharmacyIds = Object.keys(pharmacies)

    if(pharmacyIds.length > 0) {
      Promise.each(pharmacyIds, (pharmacyId) => {
        return this.bot.sendMessage(
          chatId,
          printPharmacy(pharmacies[pharmacyId], products),
          pharmacyOptions(pharmacyId)
        )
      })
    } else {
      this.bot.sendMessage(chatId, '\u{1F4E2} Basket is empty')
    }
  }

  removedFromBasket(chatId, cb) {
    this.bot.sendMessage(chatId, '\u{1F4E2} Order has been removed from basket')
      .then(cb)
  }

  orderCreated(chatId, cb) {
    this.bot.sendMessage(chatId, '\u{1F4E2} Order has been created!')
      .then(cb)
  }
}