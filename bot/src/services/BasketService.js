import redis from 'redis'

/**
 * Basket service
 */
export default class BasketService {
  /**
   * Basket service constructor
   * @param {string} host - redis host
   * @param {string|number} port - redis port
   */
  constructor(host, port) {
    this.client = redis.createClient(port, host)
  }

  /**
   * Create user basket
   * @param {string} id - chat id
   * @param {Fucntion} cb - callback function
   */
  createBasket(id, cb) {
    this.client.set(id, JSON.stringify({
      id,
      name: '',
      phone: '',
      location: null,
      pharmacies: {},
      products: {}
    }), cb)
  }

  /**
   * Return user basket
   * @param {string} id - chat id
   * @param {Function} cb - callback function
   */
  getBasket(id, cb) {
    this.client.get(id, (err, basket) => {
      if(err) return cb(err)

      const basketObj = JSON.parse(basket)
      cb(null, basketObj)
    })
  }

  /**
   * Adding product to basket
   * @param {string} id - chat id
   * @param {Object} product - product that will be added to the basket
   * @param {Function} cb - callback function
   */
  addProduct(id, product, cb) {
    const { pharmacy, name, price, _id: productId } = product
    const { _id: pharmacyId } = pharmacy

    this.client.get(id, (err, data) => {
      if (err) return cb(err)
      const basket = JSON.parse(data)

      if (pharmacyId in basket.pharmacies) {
        basket.pharmacies[pharmacyId].products.push(productId)
      } else {
        basket.pharmacies[pharmacyId] = {
          info: pharmacy,
          products: [productId]
        }
      }

      if (!(productId in basket.products)) {
        basket.products[productId] = { name, price }
      }

      this.client.set(id, JSON.stringify(basket), cb)
    })
  }

  /**
   * Setting of the Telegram user location
   * @param {string} id - chat id
   * @param {Array} location - lattitude and longitude of the user
   * @param {Function} cb - callback function
   */
  setLocation(id, location, cb) {
    this.client.get(id, (err, data) => {
      if (err) return cb(err)
      const basket = JSON.parse(data)

      basket.location = location
      this.client.set(id, JSON.stringify(basket), cb)
    })
  }

  /**
   *
   * @param {string} id - chat id
   * @param {string} name - name of user
   * @param {string} phone - phone number of user
   * @param {Function} cb - callback function
   */
  setContact(chatId, name, phone, cb) {
    this.client.get(chatId, (err, data) => {
      if (err) return cb(err)
      const basket = JSON.parse(data)

      basket.name = name
      basket.phone = phone

      this.client.set(chatId, JSON.stringify(basket), cb)
    })
  }

  /**
   *
   * @param {string} id - chat id
   * @param {string} orderId - order id
   * @param {Function} cb - callback function
   */
  removeOrder(chatId, orderId, cb) {
    this.client.get(chatId, (err, data) => {
      const basket = JSON.parse(data)

      if(orderId in basket.pharmacies) {
        delete basket.pharmacies[orderId]
      }

      this.client.set(chatId, JSON.stringify(basket), cb)
    })
  }

  /**
   *
   * @param {string} id - chat id
   * @param {string} orderId - order id
   * @param {Function} cb - callback function
   */
  getOrder(id, orderId, cb) {
    this.client.get(id, (err, data) => {
      if (err) return cb(err)

      const basket = JSON.parse(data)
      const { name, location, phone, pharmacies } = basket

      const order = {
        name,
        location,
        phone,
        products: pharmacies[orderId].products,
        pharmacy: orderId
      }

      cb(null, order)
    })
  }
}
