import request from 'request'


export default class PharmacyService {
  constructor(url) {
    this.url = url
  }

  searchProducts(text, cb) {
    request.get({
      url: `${this.url}/products/search`,
      qs: { q: text }
    }, (err, res, body) => {
      if(err){
        cb(err)
      } else if(res.statusCode !== 200) {
        cb(new Error('Invalid status'))
      } else if(res.statusCode === 200) {
        const results = JSON.parse(body)
        cb(null, results)
      }
    })
  }

  displayPharmacies(productName, location, cb) {
    const qs = { 
      name: productName
    }

    if(location) {
      qs.location = location
    }

    request.get(
      {
        url: `${this.url}/products`, 
        qs
      }, (err, res, body) => {
        if (err) 
          return cb(err)
        if (res.statusCode !== 200 )
          return cb(new Error('Invalid status'))
        
        const products = JSON.parse(body)
        cb(null, products)
    })
  }

  displayProduct(id, cb) {
    request.get(`${this.url}/products/${id}`, (err, res, body) => {
      if (err) 
        return cb(err)
      if (res.statusCode !== 200 )
        return cb(new Error('Invalid status'))

      const product = JSON.parse(body)

      cb(null, product)
    })
  }

  createOrder(order, cb) {
    request.post({ url: `${this.url}/orders`, json: order}, (err, res, body) => {
      if (err) 
        return cb(err)
      if (res.statusCode !== 204)
        return cb(new Error('Invalid status'))

      cb(null)  
    })
  }
}