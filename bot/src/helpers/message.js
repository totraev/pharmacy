export const searchMsg = (product) => (`
\u{1F48A} <b>${product._id}:</b>

<b>Producer:</b> <i>${product.producer}</i>
<b>Price:</b> from <i>${product.minPrice}</i> sum to <i>${product.maxPrice}</i> sum

<b>Choose pharmacies:</b>
`)

export const productMsg = (product) => (`
\u{1F48A} <b>${product.name}:</b>

\u{1F4B0} <b>price:</b> <i>${product.price}</i> sum
\u{2B50} <b>quantity:</b> <i>${product.quantityWH}</i>
\u{1F3ED} <b>producer:</b> <i>${product.producer}</i>
`)

export const notFoundMsg = () => ' \u{1F6AB} Nothing was found \u{1F61E}'

export const errorMsg = (msg) => msg

export const setSearchOptions = (name) => ({
  parse_mode: 'HTML',
  reply_markup: {
    inline_keyboard:[
      [{
        text: 'All',
        callback_data: JSON.stringify({ name, key: 'all' })
      },{
        text: 'Closest',
        callback_data: JSON.stringify({ name, key: 'geo' })
      }]
    ]
  }
})

export const setProductOptions = (id, pharmacyId) => ({
  parse_mode: 'HTML',
  reply_markup: {
    inline_keyboard:[
      [{
        text: 'Add to basket',
        callback_data: JSON.stringify({ id, key: 'add' })
      }]
    ]
  }
})

export const contactOptions = () => ({
  reply_markup: {
    one_time_keyboard: true,
    resize_keyboard: true,
    keyboard:[
      [{
        text: '\u{260E} Phone',
        request_contact: true
      }]
    ]
  }
})

export const locationOptions = () => ({
  reply_markup: {
    one_time_keyboard: true,
    resize_keyboard: true,
    keyboard:[
      [{
        text: '\u{1F30F} Location',
        request_location: true
      }]
    ]
  }
})

export const basketOptions = () => ({
  parse_mode: 'HTML',
  reply_markup: {
    resize_keyboard: true,
    keyboard:[['\u{1F6D2} Basket']]
  }
})

export const printPharmacy = (pharmacy, productsHash) => {
  const title = `<b>\u{1F3E5} ${pharmacy.info.name}:</b>`

  const products = pharmacy.products.reduce(
    (acc, productId, i) => acc + printProduct(productId, productsHash, i),
    ''
  )

  const totalPrice = `<b>\u{1F6CD} Total</b>: ${calcTotalPrice(pharmacy, productsHash)} sum`

  return `${title}\n${products}\n${totalPrice}`
}

const printProduct = (id, products, i) => (`
  <b>${++i}. ${products[id].name}:</b>
  \u{1F4B0}price: <i>${products[id].price} sum</i>
`)

const calcTotalPrice = (pharmacy, productsHash) =>
  pharmacy.products.reduce((price, id) =>
    price + productsHash[id].price, 0)

export const pharmacyOptions = (id) => ({
  parse_mode: 'HTML',
  reply_markup: {
    inline_keyboard:[
      [{
        text: 'Make Order',
        callback_data: JSON.stringify({ id, key: 'order' })
      },{
        text: 'Remove Order',
        callback_data: JSON.stringify({ id, key: 'remove' })
      }]
    ]
  }
})