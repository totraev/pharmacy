import angular from 'angular'
import io from 'socket.io-client'

export default class NavbarController {
  constructor($state, $http, jwtHelper, $scope, $rootScope) {
    this.$scope = $scope
    this.jwtHelper = jwtHelper
    this.$state = $state
    this.count = 0

    $http.get('/api/orders/count').then((res) => {
      this.count = res.data
      console.log(this.count)
      //$rootScope.orderCount = this.count
    })
    
    this.initSocket()
  }

  toggleAside(){
    let body = angular.element(document).find('body');

    if(body.hasClass('sidebar-collapse')){
      body.removeClass('sidebar-collapse');
    } else {
      body.addClass('sidebar-collapse');
    }
  }

  signout(){
    localStorage.removeItem('jwt')
    this.$state.go('auth.signin')
  }

  goTo() {
    this.$state.go('admin.map')
  }

  initSocket() {
    const socket = io('http://localhost', { path: '/api/sockets' })
    const { id } = this.jwtHelper.decodeToken(localStorage.getItem('jwt'))

    socket.on(id, (data) => {
      this.count++
      //$rootScope.orderCount = this.count 
      this.$scope.$apply()
    })
  }
}
