export default class MapCtrl {
  constructor($http, $state, leafletData) {
    this.$state = $state
    this.$http = $http
    this.$leafletData = leafletData
    
    this.default = {
      tileLayer: 'http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png',
      maxZoom: 14,
      path: {
          weight: 10,
          color: '#800000',
          opacity: 1
      }
    }

    this.center = {
      lat: 41.311081,
      lng: 69.240562,
      zoom: 11
    }
    this.orders = []
    this.markers = {}

    $http.get('/api/map').then((res) => {
      this.orders = res.data.map(order => {
        order.date = new Date(order.date).toLocaleString()
        return order
      })
      
      this._getMarkers()
    })
  }

  _getMarkers() {
    this.markers = this.orders.reduce((acc, order) => {
      const [lat, lng] = order.location

      acc[order._id] = {
        lat,
        lng,
        focus: true,
        draggable: false,
        message: `<b>${order.name}:</b> +${order.phone}`
      }
      return acc
    }, {})

    
    this.$leafletData.getMap().then((map) => {
      if(this.orders.length) {
        const latLngs = this.orders.map(order => order.location)
        map.fitBounds(latLngs)
      }
    })
  }

  goTo(id) {
    this.$state.go('admin.order', { id })
  }

  confirm(id) {
    this.$http.patch(`/api/orders/${id}`, { new: false }).then((res) => {
      this.$state.reload()
    })
  }
}
