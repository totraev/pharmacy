export default class ProductEditCtrl {
  constructor($http, jwtHelper, $state, $stateParams) {
    this.$http = $http
    this.$state = $state
    this.errMsg = ''
    this.$stateParams = $stateParams

    const {id, location} = jwtHelper.decodeToken(localStorage.getItem('jwt'))

    this.product = {
      name: '',
      price: '',
      quantityWH: '',
      producer: '',
      location,
      pharmacy: id
    }

    $http.get(`/api/products/${$stateParams.id}`).then((res) => {
      this.product = res.data
      console.log(this.product)
    })
  }

  onSubmit(form) {
    if(form.$invalid) return

    this.$http.patch(`/api/products/${this.$stateParams.id}`, this.product)
      .then(() => this.$state.go('admin.products'))
      .catch((res) => this.errMsg = res.data)
  }
}
