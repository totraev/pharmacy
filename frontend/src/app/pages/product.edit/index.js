import angular from 'angular'

import ProductEditCtrl from './controller'
import tpl from './product.pug'

export default angular.module('page.productedit', [])
  .controller('ProductEditCtrl', ProductEditCtrl)
  .config(($stateProvider) => {
    $stateProvider.state('admin.productedit', {
      url: '/product/:id/edit',
      template: tpl,
      controller: 'ProductEditCtrl',
      controllerAs: 'vm'
    });
  })
  .name
