export default class PharmacyCtrl {
  constructor($http, $state, $rootScope) {
    this.$http = $http
    this.$state = $state

    this.default = {
      tileLayer: 'http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png',
      maxZoom: 14,
      path: {
          weight: 10,
          color: '#800000',
          opacity: 1
      }
    }

    this.center = {
      lat: 51.505,
      lng: -0.09,
      zoom: 8
    }

    this.markers = {}

    this.pharmacy = {}

    this.$http.get('/api/pharmacy').then((res) => {
      const [lng, lat] = res.data.location

      this.pharmacy = res.data
      this.center = {
        lat,
        lng,
        zoom: 15
      }
      this.markers.pharmacy = {
        lat,
        lng,
        focus: true,
        draggable: false,
        message: `<b>${this.pharmacy.name}</b>`
      }
    })
  }

  goTo() {
    this.$state.go('admin.pharmacyedit')
  }
}
