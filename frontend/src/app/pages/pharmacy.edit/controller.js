export default class PharmacyEditCtrl {
  constructor($http, jwtHelper, $state) {
    this.$http = $http
    this.$state = $state
    this.errMsg = ''

    this.default = {
      tileLayer: 'http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png',
      maxZoom: 14
    }

    this.center = {
      lat: 0,
      lng: 0,
      zoom: 15
    }

    this.markers = {}

    const {id, location} = jwtHelper.decodeToken(localStorage.getItem('jwt'))

    this.pharmacy = {
      name: '',
      phone: '',
      address: '',
      location: [0, 0]
    }

    this.$http.get('/api/pharmacy').then((res) => {
      const { name, phone, address, location } = res.data
      const [lng, lat] = location

      this.pharmacy = { name, phone, address, location }

      this.center = {
        lat,
        lng,
        zoom: 15
      }
      this.markers.pharmacy = {
        lat,
        lng,
        focus: false,
        draggable: true,
        message: `<b>Set your location</b>`
      }
    })
  }

  onSubmit(form) {
    if(form.$invalid) return
    const { lat, lng } = this.markers.pharmacy
    this.pharmacy.location = [lng, lat]

    this.$http.patch('/api/pharmacy', this.pharmacy)
      .then((data) => this.$state.go('admin.pharmacy'))
      .catch((res) => this.errMsg = res.data)
  }
}
