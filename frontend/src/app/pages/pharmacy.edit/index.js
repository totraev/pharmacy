import angular from 'angular'

import PharmacyEditCtrl from './controller'
import tpl from './pharmacy.pug'

export default angular.module('page.pharmacyedit', [])
  .controller('PharmacyEditCtrl', PharmacyEditCtrl)
  .config(($stateProvider) => {
    $stateProvider.state('admin.pharmacyedit', {
      url: '/pharmacy/edit',
      template: tpl,
      controller: 'PharmacyEditCtrl',
      controllerAs: 'vm'
    });
  })
  .name
