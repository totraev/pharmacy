export default class OrdersCtrl {
  constructor($http, $state) {
    this.$state = $state
    this.$http = $http

    this.orders = []
    this.ordersAll = []
    this.pager = {
      currentPage: 1,
      totalItems: 0,
      maxSize: 5,
      orders: []
    }

    this.overlay = true

    $http.get('/api/orders').then((res) => {
      this.orders = res.data.map(order => {
        order.date = new Date(order.date).toLocaleString()
        return order
      })

      this.ordersAll = this.orders
      this.pager.totalItems = res.data.length
      this.currentProducts()
      this.sort()
      this.overlay = false
    })

    this.searchValue = ''
    this.searchBy = 'name'
    this.sortBy = 'date'
    this.sortOrder = 'desc'
  }

  pageChanged() {
    this.currentProducts()
  }

  currentProducts() {
    const start = (this.pager.currentPage - 1) * 10
    const end = start + 10

    this.pager.orders = this.orders.slice(start, end)
  }

  goTo(id) {
    this.$state.go('admin.order', { id })
  }

  confirm(id) {
    this.$http.patch(`/api/orders/${id}`, { new: false }).then((res) => {
      this.$state.reload()
    })
  }

  _sortAsc(a, b) {
    return a > b ? 1 : -1
  }

  _sortDesc(a, b) {
    return b > a ? 1 : -1
  }

  sort() {
    const key = this.sortBy
    const order = this.sortOrder

    if(order === 'asc'){
      this.orders = this.orders.sort((a, b) => this._sortAsc(a[key], b[key]))
    } else {
      this.orders = this.orders.sort((a, b) => this._sortDesc(a[key], b[key]))
    }

    this.currentProducts()
  }

  filter() {
    const regexp = new RegExp(this.searchValue, 'i')
    this.orders = this.ordersAll.filter(value => regexp.test(value[this.searchBy]))

    this.currentProducts()
  }
}
