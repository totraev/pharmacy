export default class OrderCtrl {
  constructor($http, $stateParams, $state) {
    this.$http = $http
    this.$state = $state
    this.default = {
      tileLayer: 'http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png',
      maxZoom: 14,
      path: {
          weight: 10,
          color: '#800000',
          opacity: 1
      }
    }

    this.center = {
      lat: 51.505,
      lng: -0.09,
      zoom: 8
    }

    this.markers = {}

    this.order = {}
    this.totalPrice = 0

    $http.get(`/api/orders/${$stateParams.id}`).then((res) => {
      this.order = res.data
      const [lat, lng] = this.order.location
      this.totalPrice = this.order.products.reduce((sum, product) => sum + product.price, 0)

      this.markers.order = {
        lat,
        lng,
        focus: true,
        draggable: false,
        message: `<b>${this.order.name}:</b> ${this.order.phone}`
      }

      this.center = {
        lat,
        lng,
        zoom: 15
      }
    })
  }

  confirm(id) {
    this.$http.patch(`/api/orders/${id}`, { new: false }).then((res) => {
      this.$state.reload()
    })
  }
}
