import modalTpl from './modal/modal.pug'

export default class ProductsCtrl {
  constructor($http, $state, $uibModal) {
    this.modal = $uibModal
    this.$state = $state
    this.$http = $http
    this.products = []
    this.productsAll = []
    this.pager = {
      currentPage: 1,
      totalItems: 0,
      maxSize: 5,
      products: []
    }
    this.overlay = true

    this.$http.get('/api/products').then((res) => {
      this.products = res.data
      this.productsAll = res.data
      this.pager.totalItems = res.data.length

      this.currentProducts()
      this.overlay = false
    })

    this.searchValue = ''
    this.searchBy = 'name'
    this.sortBy = 'name'
    this.sortOrder = 'asc'
  }

  pageChanged() {
    this.currentProducts()
  }

  currentProducts() {
    const start = (this.pager.currentPage - 1) * 10
    const end = start + 10

    this.pager.products = this.products.slice(start, end)
    this.pager.totalItems = this.products.length
  }

  goTo(id) {
    this.$state.go('admin.product', { id })
  }

  createProduct() {
    this.$state.go('admin.productcreate')
  }

  deleteProduct(id, $event) {
    $event.preventDefault()
    if(!confirm('Are you sure?')) return $event.stopPropagation()

    this.$http.delete(`/api/products/${id}`)
      .then(() => this.$state.go('admin.products'))
      .catch((res) => console.log(res.data))
  }

  editProduct(id, $event) {
    $event.preventDefault()
    this.$state.go('admin.productedit', { id })
  }

  _sortAsc(a, b) {
    return a > b ? 1 : -1
  }

  _sortDesc(a, b) {
    return b > a ? 1 : -1
  }

  sort() {
    const key = this.sortBy
    const order = this.sortOrder

    if(order === 'asc'){
      this.products = this.products.sort((a, b) => this._sortAsc(a[key], b[key]))
    } else {
      this.products = this.products.sort((a, b) => this._sortDesc(a[key], b[key]))
    }

    this.currentProducts()
  }

  filter() {
    const regexp = new RegExp(this.searchValue, 'i')
    this.products = this.productsAll.filter(value => regexp.test(value[this.searchBy]))

    this.currentProducts()
  }

  openModal() {
    this.modal.open({
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      template: modalTpl,
      controller: 'ModalCtrl',
      controllerAs: 'vm'
    })
  }
}
