import angular from 'angular'

export default class ModalCtrl{
  constructor($uibModalInstance, $http, $state) {
    this.$http = $http
    this.$uibModalInstance = $uibModalInstance
    this.$state = $state
    this.file = null
  }

  onSubmit() {
    let fd = new FormData()
    fd.append('csv', this.file)

    this.$http.post('/api/products/upload', fd, {
      transformRequest: angular.identity,
      headers: {'Content-Type': undefined}
    })
    .success(() => {
      this.$state.reload()
      this.$uibModalInstance.close()
    })
    .error(() => {
      console.log("error!!")
    })
  }

  cancel() {
    this.$uibModalInstance.close()
  }
}
