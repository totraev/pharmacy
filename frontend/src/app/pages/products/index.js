import angular from 'angular'

import ProductsCtrl from './controller'
import ModalCtrl from './modal/controller'
import fileModal from './modal/directive'
import tpl from './products.pug'

export default angular.module('page.products', [])
  .controller('ProductsCtrl', ProductsCtrl)
  .controller('ModalCtrl', ModalCtrl)
  .directive('fileModel', fileModal)
  .config(($stateProvider) => {
    $stateProvider.state('admin.products', {
      url: '/products',
      template: tpl,
      controller: 'ProductsCtrl',
      controllerAs: 'vm'
    });
  })
  .name
