import express from 'express'
import bot from '../controllers/bot'

const router = express.Router()

router
  .get('/products/search', bot.search)
  .get('/products', bot.index)
  .get('/products/:id', bot.view)
  .post('/orders', bot.create)

export default router
