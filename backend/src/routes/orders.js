import express from 'express'
import order from '../controllers/orders'

const router = express.Router()

router
	.get('/', order.index)
	.get('/count', order.count)
	.get('/:id', order.view)
	.patch('/:id', order.update)
	
export default router
