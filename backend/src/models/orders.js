import mongoose from 'mongoose'
import db from '../db'
import './products'

const { Schema } = mongoose
const { ObjectId } = Schema.Types

const OrderSchema = new Schema({
  new: {
    type: Boolean,
    default: true
  },
  name: {
    type: String,
    required: true
  },
  location: {
    type: [Number],
    required: true
  },
  phone: {
    type: String,
    required: true
  },
  products: [{
    type: ObjectId,
    ref: 'Product'
  }],
  pharmacy: {
    type: ObjectId,
    ref: 'Pharmacy',
    required: true,
    index: true
  },
  date: { type: Date, default: Date.now }
})

export default db.model('Order', OrderSchema)
