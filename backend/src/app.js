import express from 'express'

// importing middleware for express
import bodyParser from 'body-parser'
import jwt from 'express-jwt'

// importing of routes
import map from './routes/map'
import pharmacy from './routes/pharmacy'
import products from './routes/products'
import orders from './routes/orders'
import auth from './routes/auth'
import bot from './routes/bot'

// importing of route for config
import config from './config'

// creating application by express function
const app = express()

const { secret } = config.jwt
const authenticate = jwt({secret})

// calling method use of object app in order to connect middlewares with express
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

// calling method use of app to connect routers with express
app.use('/api/map', authenticate, map)
app.use('/api/pharmacy', authenticate, pharmacy)
app.use('/api/products', authenticate, products)
app.use('/api/orders', authenticate, orders)
app.use('/api/auth', auth)
app.use('/bot', bot)

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found')
  err.status = 404
  next(err)
})

// error handling for Unauthorized users
app.use(function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401).send('Unauthorized');
  } else {
    next(err)
  }
})

// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.json(res.locals)
})

export default app
