import fs from 'fs'
import csv from 'csv-parse'
import path from 'path'
import Product from '../models/products'

class CSVService {
  constructor(path, pharmacy) {
    this.pharmacy = pharmacy || {}
    this.chunkSize = 100
    this.chunk = []
    this.path = path
    this.options = {
      delimiter: ';',
      quote: '',
      columns: ['name', 'price', 'quantityWH', 'producer']
    }
  }

  parse(onFinish) {
    const { _id, location } = this.pharmacy

    return fs.createReadStream(this.path)
      .pipe(csv(this.options))

      .on('data', (data) =>  {
        this.chunk.push({
          ...data,
          location,
          pharmacy: _id
        })

        if(this.chunk.length !== this.chunkSize) return

        Product.insertMany(this.chunk, (err) => {
          if(err) throw err
        })

        this.chunk = []
      })

      .on('finish', () => {
        Product.insertMany(this.chunk, (err) => {
          if(err) throw err
        })

        onFinish()
      })
  }
}

export default CSVService
