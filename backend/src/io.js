import Server from 'socket.io'

const io = Server({ path: '/api/sockets' })

export default io