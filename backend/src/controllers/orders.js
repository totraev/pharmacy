import Order from '../models/orders'

export default {
	index(req, res) {
		const {id: pharmacy} = req.user

		Order.find({pharmacy}).populate('products').exec((err, orders) => {
			if(err) {
				res.status(500).send()
			} else {
				res.status(200).json(orders)
			}
		})
	},

	view(req, res) {
		const { id: _id } = req.params
		const { id: pharmacy } = req.user

		Order.findOne({_id, pharmacy}).populate('products').exec((err, order) => {
			if(err) {
				res.status(500).send()
			} else {
				res.status(200).json(order)
			}
		})
	},

	update(req, res) {
		const { id } = req.params
		const { body } = req

		Order.update({ _id: id }, body, { multi: false }, (err) => {
			if(err) res.status(500).send()

			res.status(200).send('ok')
		})
	},

	count(req, res) {
		const {id: pharmacy} = req.user
		
		Order.count({ new: true, pharmacy }, (err, count) => {
			if (err) res.status(500).send()

			res.status(200).json(count)
		})
	}
}
