import Order from '../models/orders'

export default {
	index(req, res) {
		const { id: pharmacy } = req.user

		Order.find({ new: true, pharmacy })
			.populate('products')
			.exec((err, orders) => {
				if(err) res.status(500).send()

				res.status(200).json(orders)
			})
	}
}