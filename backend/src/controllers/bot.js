import Product from '../models/products'
import Order from '../models/orders'
import io from '../io'

export default {
  search(req, res) {
    const { q } = req.query
    if(!q) res.status(304).send('"q" parameter is required!')

    const regex = new RegExp(`^${q}`, 'i')

    Product.aggregate([
      { $match: { name: regex }},
      { $group: { 
        _id: '$name',
        minPrice: { $min: '$price' },
        maxPrice: { $max: '$price' },
        producer: { $first: '$producer' }}},
      { $limit: 10 }
    ], (err, results) => {
      if (err) return res.status(500).send()
      res.status(200).send(results)
    })
  },

	index(req, res) {
    if(!req.query.name) res.status(304).send('"name" parameter is required!')

    const { name, location } = req.query
    const query = { 
      name: new RegExp(`^${name}`, 'i')
    }

    if(location) {
      const [lat, lng] = location
      query.location = { $geoWithin: { $centerSphere: [ [ lng, lat ], 3/3963.2 ] } }
    }

    Product.find(query)
      .populate('pharmacy')
      .exec((err, products) => {
        if(err) return res.status(500).send()

        res.status(200).json(products)
      })
	},

  view(req, res) {
    const { id } = req.params

    Product
      .findById(id)
      .populate('pharmacy')
      .exec((err, product) => {
        if(err) return res.status(500).send()

        res.status(200).json(product)
      })
  },

  create(req, res) {
    const { body } = req

    io.emit(body.pharmacy, { body })
    io.emit('test', { body })
    
    Order.create(body, (err) => {
      if(err) return res.status(401).send()

      res.status(204).send()
    })   
  }
}
