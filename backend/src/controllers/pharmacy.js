import Pharmacy from '../models/pharmacy'

export default {
	index(req, res) {
		const { id } = req.user
		
		Pharmacy.findById(id, (err, pharmacy) => {
			if(err){
				res.status(500).send(err.message)
			} else {
				res.status(200).send(pharmacy)
			}
		})
	},

	update(req, res) {
		const { id } = req.user
		const { body } = req
		const [lat, lng] = body.location
		const pharmacy = {
			...body,
			location: [lng, lat]
		}

		Pharmacy.update({ _id: id}, body, { multi: false }, (err) => {
			if(err){
				res.status(500).send(err.message)
			} else {
				res.status(200).send('ok')
			}
		})
	}
}
